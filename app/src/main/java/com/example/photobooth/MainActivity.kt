package com.example.photobooth

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.util.Size
import android.view.MotionEvent
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.camera.core.*
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import com.example.photobooth.databinding.ActivityMainBinding
import java.io.File

const val CAMERA_REQUEST = 1003

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding
    var imageCapture: ImageCapture? = null
    var videoCapture: VideoCapture? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                checkSelfPermission(Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO), CAMERA_REQUEST)
        } else {
            showCameraUI()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == CAMERA_REQUEST && grantResults.all { it == PackageManager.PERMISSION_GRANTED }) {
            showCameraUI()
        }
    }

    @SuppressLint("RestrictedApi")
    private fun startVideoRecording() {
        val videoCapture = videoCapture ?: return

        val videoFile = File(filesDir, "${System.currentTimeMillis()}.mp4")
        val outputOptions = VideoCapture.OutputFileOptions.Builder(videoFile).build()

        videoCapture.startRecording(outputOptions, ContextCompat.getMainExecutor(this), object: VideoCapture.OnVideoSavedCallback {
            override fun onVideoSaved(outputFileResults: VideoCapture.OutputFileResults) {
                val savedUri = Uri.fromFile(videoFile)
                val msg = "Video capture succeeded: $savedUri"
                Toast.makeText(baseContext, msg, Toast.LENGTH_SHORT).show()
                Log.d("sample", msg)
            }

            override fun onError(videoCaptureError: Int, message: String, cause: Throwable?) {
                Log.e("sample", "Video capture failed: ${cause?.message}", cause)
            }
        })
    }

    @SuppressLint("RestrictedApi")
    private fun stopVideoRecording() {
        val videoCapture = videoCapture ?: return
        videoCapture.stopRecording()
    }

    private fun takePhoto() {
        val imageCapture = imageCapture ?: return

        val photoFile = File(filesDir, "${System.currentTimeMillis()}.jpg")
        val outputOptions = ImageCapture.OutputFileOptions.Builder(photoFile).build()

        imageCapture.takePicture(outputOptions, ContextCompat.getMainExecutor(this), object : ImageCapture.OnImageSavedCallback {
            override fun onError(exc: ImageCaptureException) {
                Log.e("sample", "Photo capture failed: ${exc.message}", exc)
            }

            override fun onImageSaved(output: ImageCapture.OutputFileResults) {
                val savedUri = Uri.fromFile(photoFile)
                val msg = "Photo capture succeeded: $savedUri"
                Toast.makeText(baseContext, msg, Toast.LENGTH_SHORT).show()
                Log.d("sample", msg)
            }
        })
    }

    @SuppressLint("RestrictedApi")
    private fun showCameraUI() {
        val cameraProviderFuture = ProcessCameraProvider.getInstance(this)
        cameraProviderFuture.addListener({
            val cameraProvider = cameraProviderFuture.get()

            // Set up the preview use case to display camera preview.
            val preview = Preview.Builder().build()
            imageCapture = ImageCapture.Builder().build()

            val targetSize = Size(720, 1280)
            videoCapture = VideoCapture.Builder().setTargetResolution(targetSize).build()

            // Choose the camera by requiring a lens facing
            val cameraSelector = CameraSelector.Builder()
                .requireLensFacing(CameraSelector.LENS_FACING_BACK)
                .build()

            cameraProvider.bindToLifecycle(this as LifecycleOwner, cameraSelector, preview, videoCapture)

            preview.setSurfaceProvider(binding.viewFinder.surfaceProvider)
        }, ContextCompat.getMainExecutor(this))

        binding.takePhoto.setOnClickListener { takePhoto() }
        binding.switchFlash.setOnCheckedChangeListener { _, isChecked ->
            val imageCapture = imageCapture ?: return@setOnCheckedChangeListener
            imageCapture.flashMode = if (isChecked) ImageCapture.FLASH_MODE_AUTO else ImageCapture.FLASH_MODE_OFF
        }

        binding.takeVideo.setOnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_DOWN) {
                startVideoRecording()
            } else if (event.action == MotionEvent.ACTION_UP) {
                stopVideoRecording()
            }

            false
        }
    }
}